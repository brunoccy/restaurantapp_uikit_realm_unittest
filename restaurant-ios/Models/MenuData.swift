import RealmSwift


class MenuData: Object, Codable {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    var itemList = List<Item>()

    override class func primaryKey() -> String {
        return "id"
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case itemList = "items"
    }
    
    public required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        // Map your JSON Array response
        let items = try container.decodeIfPresent([Item].self, forKey: .itemList) ?? [Item()]
        itemList.append(objectsIn: items)
        
    }
}

// MARK: - Item
class Item: Object, Codable {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var price: Double = 0.0
    @objc dynamic var imageURL: String? = ""
    @objc dynamic var itemDescription: String? = ""
    @objc dynamic var compountKey = "\(NSTimeIntervalSince1970) + \(UUID().uuidString)"
    
    override class func primaryKey() -> String? {

        return "compountKey"
    }
    
    private enum CodingKeys: String, CodingKey {
        case id, name, price
        case imageURL = "image_url"
        case itemDescription = "description"
    }
    
}
