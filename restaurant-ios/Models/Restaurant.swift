//
//  Restaurant.swift
//  restaurant-ios
//
//  Created by Abraao on 19/05/20.
//  Copyright © 2020 Abraao. All rights reserved.
//

import Foundation

struct Restaurant: Codable {
    let id: Int
    let name: String
    let deliveryFee: Double
    let minimumOrderPrice: Int

    enum CodingKeys: String, CodingKey {
        case id, name
        case deliveryFee = "delivery_fee"
        case minimumOrderPrice = "minimum_order_price"
    }
}
