//
//  DatabaseManager.swift
//  restaurant-ios
//
//  Created by Bruno Chen on 08/06/20.
//  Copyright © 2020 Abraao. All rights reserved.
//

import RealmSwift
import Realm

protocol DatabaseManagerProtocol {
    func delete(item: Item)
    func fetchItems() -> [Item]
    func add(item: Item)
}

class DatabaseManager: DatabaseManagerProtocol {
    static let shared = DatabaseManager()
    
    private let realm = try! Realm()
    
    // MARK: - Lifecycle
    
    private init() {}
    
    // MARK: - Work With Objects
    
    func delete(item: Item) {
        try! self.realm.write {
            self.realm.delete(item)
        }
    }
    
    // MARK: - Work With Items
    
    func fetchItems() -> [Item] {
        return Array(realm.objects(Item.self).sorted(byKeyPath: "name", ascending: true))
    }
    
    func add(item: Item) {
        
        try! realm.write {
            realm.add(item)
        }
    }
    
}


