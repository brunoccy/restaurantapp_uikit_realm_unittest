//
//  NetworkManager.swift
//  restaurant-ios
//
//  Created by Abraao on 19/05/20.
//  Copyright © 2020 Abraao. All rights reserved.
//

import Foundation
import Alamofire

protocol NetworkManagerProtocol {
    func getRestaurant(completion: @escaping (Restaurant?) -> Void)
    func getMenu(completion: @escaping ([MenuData]?) -> Void)
    func sentOrder(data: [Item], success: @escaping(Bool) -> Void)
}

class NetworkManager: NSObject, NetworkManagerProtocol {

    private enum NetworkPath: String {
        case restaurant
        case menu
        case sentOrder
        case paymentMethods = "payment-methods"
        case openingHours = "opening-hours"
  

        static let baseURL = "https://my-json-server.typicode.com/delivery-direto/sample-api/"
        
        var url: String {
            return NetworkPath.baseURL + self.rawValue
        }
    }
    
    
    func getRestaurant(completion: @escaping (Restaurant?) -> Void) {
        AF.request(NetworkPath.restaurant.url).response { (response) in
            guard let data = response.data else {
                completion(nil)
                return
            }
            do {
                let restaurantData = try JSONDecoder().decode(Restaurant.self, from: data)
                completion(restaurantData)
            } catch {
                print(error.localizedDescription)
                completion(nil)
            }
        }
    }
    
    func getMenu(completion: @escaping ([MenuData]?) -> Void) {
        AF.request(NetworkPath.menu.url).response { (response) in
            guard let data = response.data else {
                completion(nil)
                return
            }
            do {
                let menuData = try JSONDecoder().decode([MenuData].self, from: data)
                completion(menuData)
            } catch {
                print(error.localizedDescription)
                completion(nil)
            }
        }
    }
    
    func sentOrder(data: [Item], success: @escaping(Bool) -> Void) {
        
        var itemData = [[:]]
        for item in data {
            itemData.append([item.compountKey: item.price])
        }
        
        let url = NetworkPath.sentOrder.url

        var request = URLRequest(url: URL.init(string: url)!)
        request.httpMethod = "POST"

        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let dataToSync = itemData
        request.httpBody = try! JSONSerialization.data(withJSONObject: dataToSync)

        AF.request(request).response { (response) in
            switch response.result{
            case .success:
                let statusCode: Int = (response.response?.statusCode)!
                switch statusCode{
                case 200:
                    success(true)
                    print("Sucesso")
                    break
                default:
                    success(false)
                    print("Erro no servidor")
                    break
                }
                break
            case .failure:
                success(false)
                print("Erro no servidor")
                break
            }
        }
    }
    
}

typealias JsonDictionay = [String : Any]

enum ServiceResponse {
    case success(response: JsonDictionay)
    case failure
}


