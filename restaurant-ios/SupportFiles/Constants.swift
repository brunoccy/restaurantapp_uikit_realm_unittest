//
//  Constants.swift
//  restaurant-ios
//
//  Created by Bruno Chen on 06/06/20.
//  Copyright © 2020 Abraao. All rights reserved.
//

import Foundation


struct K {
    static let scine1Title = "Menu"
    static let menuItemReuseIdentifier = "menuItemReusableCell"
    static let cartItemReuseIdentifier = "cartReusableCell"

    struct Cart {
        static let cartBarButtonTitle = "Concluir Pedido"
        static let alertControllerMinimumValueErrorTitle = "Não foi possivel concluir o pedido."
        static let alertControllerMinimumValueErrorMsg = "Pedido mínimo de "
        static let alertControllerSentOrderSuccessTitle = "Pedido Concluido"
        static let alertControllerSentOrderSuccessMsg = "O seu pedido foi concluido com sucesso. Muito obrigado pela preferência."
        static let alertControllerSentOrderErrorTitle = "Pedido Não Concluido"
        static let alertControllerSentOrderErrorMsg = "Parece que algo aconteceu com seu pedido, tente novamente"
        static let okAction = "ok"
    }
    
}
