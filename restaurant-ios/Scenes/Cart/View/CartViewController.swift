//
//  CartViewController.swift
//  restaurant-ios
//
//  Created by Abraao on 19/05/20.
//  Copyright © 2020 Abraao. All rights reserved.
//

import UIKit

class CartViewController: UITableViewController {
    
    var cartViewModel: CartViewModelProtocol?
    
    var cartItems: [Item] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        tableView.reloadData()
                
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadItems()
    }
    
     // MARK: - Config
    
    func config() {
        cartViewModel = CartViewModel()
        
        loadItems()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: K.Cart.cartBarButtonTitle, style: .plain, target: self, action: #selector(addTapped))
        
        tableView.register(CartTableViewCell.self as AnyClass, forCellReuseIdentifier: K.cartItemReuseIdentifier)
    }
    
    
    @objc func addTapped() {
        cartViewModel?.verifyOrder(cartItems: cartItems, completion: { (uIAlertController) in
            self.present(uIAlertController, animated: true)
        })
    }
    
    func loadItems() {
        cartViewModel?.loadItems(completion: { (ItemRealm) in
            cartItems = ItemRealm
            self.title = cartViewModel?.totalValue(cartItems: cartItems).toPrice()
            tableView.reloadData()
        })
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cartItems.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: K.cartItemReuseIdentifier, for: indexPath) as? CartTableViewCell
            else {
                fatalError("Cannot dequeue reusable cell")
        }
        
        cell.fill(with: cartItems[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alertController = UIAlertController(title: "Deseja retirar o item?", message: "", preferredStyle: .alert)
        
        let DeleteAction = UIAlertAction(title: "Sim", style: .default) { (action) in
            self.cartViewModel?.deleteItem(self.cartItems[indexPath.row])
            self.loadItems()
        }
        let OKAction = UIAlertAction(title: "Não", style: .default) { (action) in
            
        }
        alertController.addAction(DeleteAction)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)

    }
    
    
}

