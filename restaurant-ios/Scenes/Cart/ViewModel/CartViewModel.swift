//
//  CartViewTable.swift
//  restaurant-ios
//
//  Created by Bruno Chen on 08/06/20.
//  Copyright © 2020 Abraao. All rights reserved.
//

import UIKit

protocol CartViewModelProtocol {
    func totalValue(cartItems: [Item]) -> Double
    func verifyOrder(cartItems: [Item], completion: @escaping (UIAlertController) -> Void)
    
    func loadItems(completion: ([Item]) -> Void)
    func deleteItem(_ item: Item)
}

class CartViewModel: CartViewModelProtocol {
    
    var networkManager: NetworkManagerProtocol! = NetworkManager()
    
    var productsDidFetch: (() -> Void)?
    
    private(set) var items = [Any]()
    
    private var objectsOfItem = [Item]() {
        didSet {
            items = objectsOfItem.map { ($0.id, $0.name, $0.price, $0.imageURL, $0.itemDescription) }
        }
    }
    
    func totalValue(cartItems: [Item]) -> Double {
        var finalPrice: Double = 0.00
        for item in cartItems {
            finalPrice += item.price
        }
        return finalPrice
    }
    
    func verifyOrder(cartItems: [Item], completion: @escaping (UIAlertController) -> Void) {
        networkManager.getRestaurant { (restaurant) in
            if Double(restaurant?.minimumOrderPrice ?? 0) > self.totalValue(cartItems: cartItems) {
                let alertController = UIAlertController(title: K.Cart.alertControllerMinimumValueErrorTitle, message: K.Cart.alertControllerMinimumValueErrorMsg + String((Double(restaurant?.minimumOrderPrice ?? 1).toPrice())), preferredStyle: .alert)

                let OKAction = UIAlertAction(title: K.Cart.okAction, style: .default) { (action) in

                }
                alertController.addAction(OKAction)
                completion(alertController)
            } else {
                self.networkManager.sentOrder(data: cartItems) { (success) in
                    let alertController: UIAlertController
                    if success == true{
                        alertController = UIAlertController(title: K.Cart.alertControllerSentOrderSuccessTitle, message: K.Cart.alertControllerSentOrderSuccessMsg, preferredStyle: .alert)
                    } else {
                        alertController = UIAlertController(title: K.Cart.alertControllerSentOrderErrorTitle, message: K.Cart.alertControllerSentOrderErrorMsg, preferredStyle: .alert)
                    }
                    
                    let OKAction = UIAlertAction(title: K.Cart.okAction, style: .default) { (action) in

                    }
                    alertController.addAction(OKAction)
                    completion(alertController)
                    
                }
            }
        }
    }
    
    func loadItems(completion: ([Item]) -> Void)  {
        objectsOfItem = DatabaseManager.shared.fetchItems()
        completion(objectsOfItem)
    }
    
    func deleteItem(_ item: Item) {
        DatabaseManager.shared.delete(item: item)
    }
    
}
