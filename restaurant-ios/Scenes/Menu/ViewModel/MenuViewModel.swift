//
//  MenuViewModel.swift
//  restaurant-ios
//
//  Created by Bruno Chen on 08/06/20.
//  Copyright © 2020 Abraao. All rights reserved.
//

import Foundation

protocol MenuViewModelProtocol {
    func getMenu(completion: @escaping ([MenuData]?) -> Void)
    func saveItem(item: Item)
}

class MenuViewModel: MenuViewModelProtocol {
    
    var networkManager: NetworkManagerProtocol?
    
    private var objectOfItem: Item!
    
    init() {
        networkManager = networkManager ?? NetworkManager()
    }
    
    // MARK: - Callback input
    
    func getMenu(completion: @escaping ([MenuData]?) -> Void) {
        guard let network = networkManager else {return}
        
        network.getMenu { (response) in
            if let menuData = response {
                completion(menuData)
            } else {
                completion(nil)
                return
            }
        }
        
    }
    
    
    // MARK: - Output Database
    
    func saveItem(item: Item) {
        DatabaseManager.shared.add(item: item)
    }
    
}
