//
//  MenuViewController.swift
//  restaurant-ios
//
//  Created by Abraao on 19/05/20.
//  Copyright © 2020 Abraao. All rights reserved.
//

import UIKit

class MenuViewController: UITableViewController {
    
    var menuViewModel: MenuViewModelProtocol!
    
    var menuData: [MenuData]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewConfig()
    }
    
    // MARK: - View Config
    
    private func viewConfig() {
        title = K.scine1Title
        menuViewModel = MenuViewModel()
        tableView.register(MenuItemTableViewCell.self as AnyClass, forCellReuseIdentifier: K.menuItemReuseIdentifier)
        loadMenu()
        
    }
    
    // MARK: - Input
    
    func loadMenu() {
        menuViewModel?.getMenu(completion: { (menuData) in
            guard let existMenuData = menuData else {
                self.alertNoConection()
                return print("Erro de conexão")
            }
            self.menuData = existMenuData
            self.tableView.reloadData()
        })
    }
    
    func alertNoConection() {
        let alertController = UIAlertController(title: "Sem conexão", message: "Tente novamente mais tarde", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "ok", style: .default) { (action) in
            
        }
        alertController.addAction(OKAction)
        present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Output
    
    func transfer(item: Item) {
        menuViewModel.saveItem(item: item)
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return menuData?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return menuData[section].itemList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: K.menuItemReuseIdentifier, for: indexPath) as? MenuItemTableViewCell else {
            fatalError("Cannot dequeue reusable cell")
        }
        cell.fill(with: menuData[indexPath.section].itemList[indexPath.row])
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

            let v = UIView(frame: CGRect(x: 0, y:0, width: tableView.frame.width, height: 30))
            v.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            let label = UILabel(frame: CGRect(x: 8.0, y: 4.0, width: v.bounds.size.width - 16.0, height: v.bounds.size.height - 8.0))
            label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            label.text = menuData[section].name
            v.addSubview(label)
            return v
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        transfer(item: menuData[indexPath.section].itemList[indexPath.row])
        loadMenu()
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
}

