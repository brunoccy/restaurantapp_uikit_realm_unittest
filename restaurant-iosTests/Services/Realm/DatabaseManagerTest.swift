//
//  RealmTest.swift
//  restaurant-iosTests
//
//  Created by Bruno Chen on 09/06/20.
//  Copyright © 2020 Abraao. All rights reserved.
//

import XCTest
@testable import restaurant_ios
//
//class RealmTest: XCTestCase {
//    
//    // MARK: - Set Up
//    override func setUpWithError() throws {
//        Realm.Configuration.defaultConfiguration.inMemoryIdentifier = self.name
//        let realm = try! Realm()
//        try! realm.write {
//          realm.deleteAll()
//    }
//    }
//
//    // MARK: - Tear Down
//    override func tearDownWithError() throws {
//        // Put teardown code here. This method is called after the invocation of each test method in the class.
//    }
//    
//    func testInitializeRealm() {
//        let person = Person(name: "name A", age: 18)
//        XCTAssertTrue((person.name) == "name A")
//        XCTAssertTrue((person.age) == 18)
//    }
//}
//
//class Person: Object {
//  dynamic var name = ""
//  dynamic var age = 0
//
//  convenience init(name: String, age: Int) {
//    self.init()
//    self.name = name
//    self.age = age
//  }
//}
