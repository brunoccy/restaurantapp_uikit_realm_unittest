//
//  CartViewModelTest.swift
//  restaurant-iosTests
//
//  Created by Bruno Chen on 09/06/20.
//  Copyright © 2020 Abraao. All rights reserved.
//

import XCTest
@testable import restaurant_ios


class CartViewModelTest: XCTestCase {

    var cartViewModel: CartViewModel!
    
    // MARK: - SetUp
    override func setUpWithError() throws {
        cartViewModel = CartViewModel()
    }
    // MARK: - Tear Down
    override func tearDownWithError() throws {
        
    }
    
    // MARK: - Tests
    func testVerifyMinimumOrderSuccesss(){
        let networkMock = NetworkManagerMock()
        cartViewModel.networkManager = networkMock
        
        let item = Item()
        item.id = 10
        item.name = "name"
        let priceTest = 70.0
        item.price = priceTest
        let descriptionTest = "Description"
        item.itemDescription = descriptionTest
        item.imageURL = ""
        
        cartViewModel.verifyOrder(cartItems: [item], completion: { (uIAlertController) in
            XCTAssertTrue(uIAlertController.title == K.Cart.alertControllerSentOrderSuccessTitle)
        })
    }
    
    func testVerifyMinimumOrderError(){
        let networkMock = NetworkManagerMock()
        cartViewModel.networkManager = networkMock
        
        let item = Item()
        item.id = 10
        item.name = "name"
        let priceTest = 40.0
        item.price = priceTest
        let descriptionTest = "Description"
        item.itemDescription = descriptionTest
        item.imageURL = ""
        
        cartViewModel.verifyOrder(cartItems: [item], completion: { (uIAlertController) in
            XCTAssertTrue(uIAlertController.title == K.Cart.alertControllerMinimumValueErrorTitle)
        })
    }

    
}

// MARK: - Network Mock

class DatabaseManager: DatabaseManagerProtocol {

    
    var called: Bool = false
    var item: [Item] = []
    
    func delete(item: Item) {
        called = true
    }
    
    func fetchItems() -> [Item] {
        called = true
        return item
    }
    
    func add(item: Item) {
        called = true
    }
    
}
