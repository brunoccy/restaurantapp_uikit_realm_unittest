
//
//  CartViewControllerTest.swift
//  restaurant-iosTests
//
//  Created by Bruno Chen on 09/06/20.
//  Copyright © 2020 Abraao. All rights reserved.
//

import XCTest
@testable import restaurant_ios

class CartViewControllerTest: XCTestCase {
    
    var cartViewController: CartViewController!

    // MARK: - Set Up
    override func setUpWithError() throws {
        cartViewController = CartViewController()
    }
    // MARK: - Tear Down
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    // MARK: - Tests
    
    func testConfig() {}
    
    func testIfGetMenuIsCalled() {
        let cartViewModelMock = CartViewModelMock()
        cartViewController.cartViewModel = cartViewModelMock
        
        cartViewController.loadItems()
        
        XCTAssertTrue(cartViewModelMock.called)
    }
    
    func testMenuDataFillSuccess() {
        let cartViewModelMock = CartViewModelMock()
        cartViewController.cartViewModel = cartViewModelMock
        
        cartViewController.loadItems()
        
        XCTAssertTrue(cartViewModelMock.cartItems == cartViewModelMock.cartItems)
    }
    

}

// MARK: - Cart View Model Mock

class CartViewModelMock: CartViewModelProtocol {
    
    var called: Bool = false
    
    var cartItems: [Item] = [Item()]
    
    func totalValue(cartItems: [Item]) -> Double {
        called = true
        return 1.0
    }
    
    func verifyOrder(cartItems: [Item], completion: @escaping (UIAlertController) -> Void) {
        called = true
    }

    func loadItems(completion: ([Item]) -> Void) {
        called = true
    }
    
    func deleteItem(_ item: Item) {
        called = true
    }
    
}
