//
//  MenuViewControllerTest.swift
//  restaurant-iosTests
//
//  Created by Bruno Chen on 09/06/20.
//  Copyright © 2020 Abraao. All rights reserved.
//

import XCTest
@testable import restaurant_ios

class MenuViewControllerTest: XCTestCase {
    
    var menuViewController: MenuViewController!

    // MARK: - Set Up
    override func setUpWithError() throws {
        menuViewController = MenuViewController()
    }

    // MARK: - Tear Down
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // MARK: - Tests
    
    func testConfig() {}
    
    func testIfGetMenuIsCalled() {
        let menuViewModelMock = MenuViewModelMock()
        menuViewController.menuViewModel = menuViewModelMock
        
        menuViewController.loadMenu()
        
        XCTAssertTrue(menuViewModelMock.called)
    }
    
    func testMenuDataFillSuccess() {
        let menuViewModelMock = MenuViewModelMock()
        menuViewController.menuViewModel = menuViewModelMock
        
        menuViewController.loadMenu()
        
        XCTAssertTrue(menuViewController.menuData == menuViewModelMock.menuData)
    }
    

}

// MARK: - Menu View Model Mock

class MenuViewModelMock: MenuViewModelProtocol {
    
    var called: Bool = false
    var menuData: [MenuData]! = [MenuData()]
    
    func getMenu(completion: @escaping ([MenuData]?) -> Void) {
        completion(menuData)
        called = true
    }
    
    func saveItem(item: Item) {
        called = true
    }
}
