//
//  MenuItemTableViewCellTest.swift
//  restaurant-iosTests
//
//  Created by Bruno Chen on 09/06/20.
//  Copyright © 2020 Abraao. All rights reserved.
//

import XCTest
@testable import restaurant_ios

class MenuItemTableViewCellTest: XCTestCase {

    var menuItemTableViewCell: MenuItemTableViewCell!

    // MARK: - Set Up
    override func setUpWithError() throws {
        menuItemTableViewCell = MenuItemTableViewCell()
    }
    // MARK: - Tear Down
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // MARK: - Tests
    
    func testCellFill() {
        let item = Item()
        item.id = 10
        item.name = "name"
        let priceTest = 1.0
        item.price = priceTest
        let descriptionTest = "Description"
        item.itemDescription = descriptionTest
        item.imageURL = ""
        
        menuItemTableViewCell.fill(with: item)
        
        
        XCTAssertEqual("name", menuItemTableViewCell.textLabel?.text)
        XCTAssertEqual(priceTest.toPrice() + " • " + descriptionTest, menuItemTableViewCell.detailTextLabel?.text)
    }

}
