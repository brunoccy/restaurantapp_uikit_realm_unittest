//
//  MenuViewModelTest.swift
//  restaurant-iosTests
//
//  Created by Bruno Chen on 09/06/20.
//  Copyright © 2020 Abraao. All rights reserved.
//

import XCTest
@testable import restaurant_ios


class MenuViewModelTest: XCTestCase {

    var menuViewModel: MenuViewModel!
    
    // MARK: - SetUp
    override func setUpWithError() throws {
        menuViewModel = MenuViewModel()
    }
    // MARK: - Tear Down
    override func tearDownWithError() throws {
        
    }
    
    // MARK: - Tests
    func testIfGetMenuIsCalled(){
        let networkMock = NetworkManagerMock()
        menuViewModel.networkManager = networkMock
        
        menuViewModel.getMenu { (arrayMenuData) in
            XCTAssertTrue(networkMock.called)
        }
    }
    
    func testSaveItems(){
        let networkMock = NetworkManagerMock()
        menuViewModel.networkManager = networkMock
        
        menuViewModel.getMenu { (arrayMenuData) in
            XCTAssertTrue(networkMock.called)
        }
    }
    
}

// MARK: - Network Mock

class NetworkManagerMock: NetworkManagerProtocol {
    
    var called: Bool = false
    
    func getRestaurant(completion: @escaping (Restaurant?) -> Void) {
        called = true
        completion(Restaurant(id: 10, name: "name", deliveryFee: 1.0, minimumOrderPrice: 50))
    }
    
    func getMenu(completion: @escaping ([MenuData]?) -> Void) {
        called = true
        completion(nil)
    }
    
    func sentOrder(data: [Item], success: @escaping (Bool) -> Void) {
        called = true
        success(true)
    }
    
    
}
